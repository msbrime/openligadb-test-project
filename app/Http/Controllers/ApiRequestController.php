<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\OpenligadbSDK as Sdk;
use Illuminate\Support\Facades\Cache;
use App\Library\DataModels\Team;
use App\Library\DataModels\ApiDataModel;
use App\Library\DataModels\Match;
use Carbon\Carbon;

class ApiRequestController extends Controller
{
    protected $sdk;
    
    /**
     * Long names of the leagues
     * @var array 
     */
    protected $leagueNames = [
        "bl1" => "bundesliga 1",
        "bl2" => "bundesliga 2",
        "bl3" => "bundesliga 3",
    ];
    
    public function __construct(Sdk $sdk){
        $this->sdk = $sdk;
    }
    
    /**
     * Returns the corresponding year of the season
     * 
     * @return int
     */
    protected function season(){
        $date = Carbon::now("UTC");   
        if ($date->month >= 8){
            return $date->year;
        }
        return ($date->year - 1);
    }
    
    /**
     * Returns the participating teams in a particular league for
     * a particular season
     * 
     * @param string $league
     * @param int $year
     * @return array<object>
     */
    protected function availableTeams($league,$year){
        
        $teams = Cache::rememberForever(
            "teams{$league}{$year}"
            ,function () use ($league,$year){
                return $this->sdk->availableTeams($league,$year);
            }
        );
        
        if(count($teams < 1)){
            Cache::forget("teams{$league}{$year}");
        }
        
        return $teams;        
    }
    
    /**
     * Returns the total number of matchdays for a given league for a season
     * 
     * @param string $league
     * @param int $year
     * @return int
     */
    protected function totalMatchdays($league,$year){
        $teams = $this->availableTeams($league, $year);
        $matchdayCount = (count($teams) - 1) * 2;
        return $matchdayCount;
    }
    
    /**
     * Returns the current matchday/group for a league
     * 
     * @param string $league
     * @return object|null
     */
    protected function currentGroup($league){
        $currentGroup = Cache::remember(
            "order{$league}"
           ,240
           ,function () use ($league){
                return $this->sdk->currentGroup($league);
        });
        
        if(empty($currentGroup)){
            Cache::forget("order{$league}");
        }
        
        return $currentGroup;
    }
    
    
    /**
     * Returns a team in a league
     * 
     * @param string $league
     * @param int $id
     * @return object
     */
    protected function team($league,$id){
        $teams = $this->availableTeams($league,$this->season());
        
        $filteredTeams = array_filter($teams, function($team) use($id){
            return $team->TeamId == $id;
        });
        
        if(count($filteredTeams) > 0){
            return array_pop($filteredTeams);
        }
        
        return null;
    }
    
    /**
     * Returns an array of matches that a team has been involved in by league
     * by year
     * 
     * @param string $league
     * @param int $year
     * @param string $teamId
     * @return array<Match>
     */
    protected function matchesPlayedByTeam($league,$year,$teamId){
        $currentGroup = $this->currentGroup($league);
        $matches = $this->dataUptoMatchday(
            $league
           ,$year
           ,floor($currentGroup->GroupOrderID)
        );
        
        $teamMatches = array_filter($matches,function($match) use ($teamId){
           return $match->Team1->TeamId == $teamId || $match->Team2->TeamId == $teamId; 
        });
        
        return array_map([$this,"toMatchModel"],$teamMatches);
    }
    
    /**
     * Checks if the data for a league, for a year, for a matchday has
     * changed
     * 
     * @param string $league
     * @param int $year
     * @param int $matchday
     * @return boolean
     */
    protected function dataHasChanged($league,$year,$matchday){
       $changed = false;
       $date = $this->sdk->dataLastChanged($league, $year, $matchday);
       $lastChanged = Cache::get(
           "changed{$league}{$year}{$matchday}"
           ,null
       );

       if(!empty($date)){
           $changed = ($date != $lastChanged);
       }
       
       if($changed === true){
           Cache::forever("changed{$league}{$year}{$matchday}",$date);
       }
       
       return $changed;
    }
    
    /**
     * Returns all the matches for a league, for a season up to a matchday
     * 
     * @param string $league
     * @param int $year
     * @param int $matchday
     * @return array<object>
     */
    protected function dataUptoMatchday($league,$year,$matchday){ 

        // if($this->dataHasChanged($league, $year, $matchday) || 
        //     !Cache::has("upto{$league}{$year}{$matchday}")) {
        // }    
        $matchdata = $this->sdk->seasonMatchData($league, $year);

        if(count($matchdata) > 0){
            $filteredMatches = 
                array_filter(
                    $matchdata
                   ,function($match) use ($matchday){
                        return ((integer) $match->Group->GroupOrderID) <=  
                            ((integer) $matchday);
                    }
                );

            return $filteredMatches; 
            //Cache::forever("upto{$league}{$year}{$matchday}",$filteredMatches);
        }
         
        return [];
        //return Cache::get("upto{$league}{$year}{$matchday}",[]);
    }
    
    /**
     * Returns the data for a league, for a year, for a specific matchday
     * 
     * @param string $league
     * @param int $year
     * @param int $matchday
     * @return array<object>
     */
    protected function dataForMatchday($league,$year,$matchday){
        
        //if($this->dataHasChanged($league, $year, $matchday)){
            $matchdata = $this->sdk->matchData($league, $year,$matchday);  
            if(count($matchdata) > 0){
               // Cache::forever("for{$league}{$year}{$matchday}",$matchdata);
               return $matchdata;
            }
            
        //}
            
        //return Cache::get("for{$league}{$year}{$matchday}",[]);
          return [];
    }
    
    /**
     * Transforms match data into a Match instance
     * @param object $data
     * @return \App\Library\DataModels\Match
     */
    protected function toMatchModel($data){
        return ApiDataModel::toModel(Match::class,$data);
    }
    
    /**
     * Transforms match data into a Team instance
     * @param object $data
     * @return \App\Library\DataModels\Team
     */
    protected function toTeamModel($data){
        return ApiDataModel::toModel(Team::class, $data);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\DataModels\Match;

class HomeController extends ApiRequestController {
    
    /**
     * Displays all the results from the current matchday and shows the 
     * fixtures for the next matchday
     * 
     * @return \Illuminate\View\View
     */
    public function index() {

        try {
            
            $bl1Matches = $this->getMatchData('bl1');
            $bl2Matches = $this->getMatchData('bl2');
            $bl3Matches = $this->getMatchData('bl3');

            return view('dashboard', compact('bl1Matches', 'bl2Matches', 'bl3Matches'));
            
        } catch (\Exception $ex) {
            abort(503);
        }
    }

    /**
     * Returns an object that holds the matches for current matchday and the
     * next matchday
     * 
     * @param string $league
     * @return object
     */
    private function getMatchData($league) {
        $matchData = new \stdClass();
        $currentGroup = $this->currentGroup($league);
        $matchday = floor($currentGroup->GroupOrderID);
        $season = $this->season();
        $current = $this->dataForMatchday($league, $season, $matchday);
        $upcoming = $this->dataForMatchday($league, $season, ($matchday + 1));

        $matchData->current = array_map([$this, "toMatchModel"], $current);
        $matchData->next = array_map([$this, "toMatchModel"], $upcoming);
        $matchData->matchday = $matchday;

        return $matchData;
    }

}

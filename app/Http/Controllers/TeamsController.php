<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\DataModels\Team;

class TeamsController extends ApiRequestController {
    
    /**
     * Displays all the teams for a league
     * 
     * @param string $league
     * @return Illuminate\View\View
     */
    public function index($league = "bl1") {

        try {
            $leagueName = $this->leagueNames[$league];

            $teams = array_map(
                [$this, "toTeamModel"]
                , $this->availableTeams($league, $this->season())
            );

            return view("teams", compact("teams", "league", "leagueName"));
        } catch (\Exception $e) {
            abort(503);
        }
    }
    
    /**
     * Displays the statistics and results for a team in a league
     * 
     * @param string $league
     * @param int $id
     * @return type
     */
    public function show($league, $id) {

        try {
            $team = $this->team($league, $id);

            if (!empty($team)) {
                $team = new Team($team);
                $matches = $this->matchesPlayedByTeam($league, $this->season(), $id);
                $stats = $team->statsFromMatchData($matches);
                return view("team", compact("team", "stats", "matches"));
            }
            abort(503);
        } catch (\Exception $e) {
            abort(503);
        }
    }

}

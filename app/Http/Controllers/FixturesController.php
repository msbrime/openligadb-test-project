<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FixturesController extends ApiRequestController {
    
    /**
     * Displays the fixtures/results for a league for a matchday
     * 
     * @param string $league
     * @param int $matchday
     * @return Illuminate\View\View
     */
    public function index($league, $matchday = 1) {

        try {
            $season = $this->season();
            $matchdayCount = $this->totalMatchdays($league, $season);
            $matchdata = $this->dataForMatchday($league, $season, $matchday);
            $matches = array_map([$this, "toMatchModel"], $matchdata);
            $leagueName = $this->leagueNames[$league];

            return view(
                "fixtures"
                , compact('matchdayCount', 'matches', 'leagueName', 'matchday', 'league')
            );
        } catch (\Exception $e) {
          abort(503);  
        }
    }

}

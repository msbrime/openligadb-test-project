<?php

namespace App\Library\DataModels;

use Carbon\Carbon;

/**
 * Description of Match
 *
 * @author Salis
 */
class Match extends ApiDataModel{
    
    /**
     * Determines if the match is over / has been played already
     * 
     * @return boolean
     */
    public function played(){
        return $this->data->MatchIsFinished;
    }
    
    /**
     * Returns the formatted date of the match
     * @return string
     */
    public function date(){
        $date = Carbon::parse($this->data->MatchDateTimeUTC);
        return $date->format('jS F, Y');
    }
    
    /**
     * Returns the formatted UTC time of the match
     * @return string
     */
    public function time(){
        $date = Carbon::parse($this->data->MatchDateTimeUTC);
        return $date->format('h:i A');
    }
    
    /**
     * Returns the id of the home team 
     * @return int
     */
    public function homeTeam(){
        return $this->data->Team1;
    }
    
    /**
     * Returns the id of the away team
     * @return string
     */
    public function awayTeam(){
        return $this->data->Team2;
    }
    
    /**
     * Returns the number of goals scored by the home team
     * 
     * @return int
     */
    public function homeScore(){
       return $this->teamScore("1");
    }
    
    /**
     * Returns the number of goals scoredd by the away team
     *  
     * @return int
     */
    public function awayScore(){
        return $this->teamScore("2");
    }
    
    /**
     * Returns the id of the winnig team, 0 if the match was a draw or -1 if 
     * the game has not played or isn't finished
     * 
     * @return int
     */
    public function winner(){
        if($this->data->MatchIsFinished){
            $homeWon = $this->homeScore() > $this->awayScore();
            $drawn = $this->homeScore() === $this->awayScore();
            
            if($homeWon){
                return $this->homeTeam()->TeamId;
            }
            elseif ($drawn) {
                return 0;
            }
            else{
                return $this->awayTeam()->TeamId;
            }
        }
        
        return -1;
    }
    
    /**
     * Returns an object of statistics of a team if they were involved in a match or 
     * a boolean otherwise
     * 
     * @param type $teamId
     * @return object | boolean
     */
    public function getTeamStats($teamId){
        $isTeam1 = $teamId == $this->homeTeam()->TeamId;
        $isTeam2 = $teamId == $this->awayTeam()->TeamId;
        
        if($isTeam1 || $isTeam2){
            
            $stats = new \stdClass;
            $points = 0;
            $teamNumber = $isTeam1 ? "1" : "2";
            $oppositeNumber = $isTeam1 ? "2" : "1";
            $winner = $this->winner();
            
            $stats->for = $this->teamScore($teamNumber);
            $stats->against = $this->teamScore($oppositeNumber);
            $stats->won = $teamId ==  $winner;
            $stats->drew = $winner == 0;
            $stats->lost = !$stats->won && !$stats->drew && $winner !== -1;
            
            if($stats->won || $stats->drew){
                $points = $stats->won ? 3 : 1;
            }
            
            $stats->points = $points;
            
            return $stats;
        }
        
        return false;
    }
    

    
    /**
     * Returns the score of a team 
     * 
     * @param int $team
     * @return int
     */
    private function teamScore($team){
        if($this->played()){
            return $this->data->MatchResults[1]->{"PointsTeam{$team}"};
        }
        else{
            return ( 
                (count($this->data->MatchResults) > 0 ) ? 
                 $this->data->MatchResults[0]->{"PointsTeam{$team}"} :
                 -1
            );
        }
    }
     
}

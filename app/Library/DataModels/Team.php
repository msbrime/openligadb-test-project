<?php

namespace App\Library\DataModels;

/**
 * Description of Team
 *
 * @author Salis
 */
class Team extends ApiDataModel {
    
    /**
     * Returns the id of the team
     * 
     * @return int
     */
    public function id(){
        return $this->data->TeamId;
    }
    
    /**
     * returns the name of the team
     * 
     * @return string
     */
    public function name(){
        return $this->data->TeamName;
    }
    
    /**
     * Returns the url to the team logo
     * 
     * @return string
     */
    public function iconUrl(){
        return $this->data->TeamIconUrl;
    }
    
    /**
     * Given an array of matches, returns the statistics for the team
     * 
     * @param array<\App\Library\DataModels\Match> $matchData
     * @return object
     */
    public function statsFromMatchData($matchData){
       $overall = self::defaultStatsObject();
       
        foreach($matchData as $match){
            $stats = $match->getTeamStats($this->id());
            if($stats){
                $overall->played++;
                $overall->for += $stats->for;
                $overall->points += $stats->points;
                $overall->against += $stats->against;
               
                if($stats->won){ $overall->won++; }
                if($stats->lost){ $overall->lost++; }
                if($stats->drew){ $overall->drew++; }
            }
        }
       
       if($overall->played > 0){
            $overall->winRatio =  
                round (($overall->won / $overall->played ) * 100,2);
            $overall->lossRatio = 
                round (( $overall->lost / $overall->played ) * 100,2);
            $overall->drawRatio = 
                round (( $overall->drew / $overall->played ) * 100,2);
       }
       return $overall;
    }
    
    /**
     * Returns the default statistics object
     * @return object
     */
    private static function defaultStatsObject(){
        $stats = [
            "for" => 0,
            "against" => 0,
            "played" => 0,
            "won" => 0,
            "lost" => 0,
            "points" => 0,
            "drew" => 0,
            "winRatio" => 0,
            "lossRatio" => 0,
            "drawRatio" => 0
        ];
        
        return (object) $stats;
    }
}

<?php

namespace App\Library\DataModels;

/**
 * Description of ApiDataModel
 * 
 * @author Salis
 */
abstract class ApiDataModel {
    
    /**
     * json decoded api data
     * @var object
     */
    protected $data;
    
    public function __construct($dataObject){
        $this->data = $dataObject;
    }
    
    /**
     * Convert api data into an ApiDataModel instance
     * 
     * @param string name of ApiDataModel class
     * @param object $dataObject
     * @return ApiDataModel
     */
    public static function toModel($model,$dataObject){
        return (new $model($dataObject));
    }
    
}

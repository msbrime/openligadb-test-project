<?php

namespace App\Library\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Ring\Exception\ConnectException;
use App\Library\Response\ApiResponse;

trait MakesApiRequests
{

    /**
     * The base url for API requests
     *
     * @var string
     */
    protected $api_url;

    /**
     * Perform a get request to $url with the provided settings,$options 
     * 
     * @param  string $url
     * @param  array $options 
     * @return ApiResponse
     */
    protected function fetch($url, $options = [])
    {

        try {
            $client = new Client();
            $response = $client->get(
                "{$this->api_url}/$url", $options
            );
            
            return $this->handleSuccess($response);
        } 
        
        catch (ClientException $e) {
            return $this->handleFailure($e);
        } 
        
        catch (ServerException $e) {
            return $this->handleFailure($e);
        } 
        
        catch (RequestException $e) {
            abort(503);
        } 
        
        catch (\GuzzleHttp\Ring\Exception\ConnectException $e) {
            abort(503);
        }
    }

    /**
     * Performs multiple get requests and return the responses
     * 
     * @param  array $requests An array of urls and request options
     * @return array An array of GuzzleHttp\Message\ResponseInterface objects
     */
    protected function fetchMultiple($requests)
    {

        $requestVariables = array_keys($requests);
        $requestGroup = [];
        $client = new Client();

        foreach ($requests as $request) {
            $requestGroup[] = $client->createRequest(
                "GET", "{$this->api_url}/" . $request['url'], (array_key_exists('options', $request)) ? $request['options'] : []
            );
        }

        $results = Pool::batch($client, $requestGroup);

        return $this->handleBatchResults($requestVariables, $results);
    }

    /**
     * Perform a post request to $url with the provided settings,$options 
     * 
     * @param  string $url
     * @param  array $options 
     * @return ApiResponse
     */
    protected function post($url, $options = [])
    {
        try {
            $client = new Client();
            $response = $client->post(
                "{$this->api_url}/$url", $options
            );

            return $this->handleSuccess($response);
        } catch (ClientException $e) {
            return $this->handleFailure($e);
        } catch (ServerException $e) {
            return $this->handleFailure($e);
        } catch (RequestException $e) {
            abort(503);
        } catch (ConnectException $e) {
            abort(503);
        }
    }

    /**
     * Handler for failed requests
     * 
     * @param  \GuzzleHttp\Exception\RequestException $error
     * @return ApiResponse
     */
    protected function handleFailure(RequestException $error)
    {

        $requestResponse = $error->getResponse();

        if (!$requestResponse) {
            $requestResponse = new Response(503);
        }

        return $requestResponse;
    }

    /**
     * Handler for successful requests
     * 
     * @param  Response $response
     * @return ApiResponse
     */
    protected function handleSuccess(Response $response)
    {
        return $response;
    }

    /**
     * Handler for batched requests
     * 
     * @param  array $requestVariables 
     * @param  array $responses an array of the responses from each request
     * @return \stdClass
     */
    protected function handleBatchResults($requestVariables, $responses)
    {

        $groupResponses = [];
        $counter = 0;

        foreach ($$responses->getIterator() as $response) {
            if ($response instanceof Response) {
                // pass the response through the success
                // handler if it is an actual response
                $groupResponses[$requestVariables[$counter]] = $this->handleSuccess($response);
            } else {
                // otherwise pass it through the fail handler
                $groupResponses[$requestVariables[$counter]] = $this->handleFailure($response);
            }
            $counter++;
        }

        return (object) $groupResponses;
    }

}

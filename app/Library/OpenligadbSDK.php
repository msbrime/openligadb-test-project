<?php

namespace App\Library;

/**
 * Description of OpenligadbSDK
 *
 * @author Salis
 */
class OpenligadbSDK {
    
    use Traits\MakesApiRequests {
        fetch as traitFetch;
    }
    
    /**
     * the default headers for every reuest
     * 
     * @var array
     */
    private $defaultHeaders = [
        "Accept" => "content/json"
    ];
    
    public function __construct() {
        // sets the base url for every request
        $this->api_url = "https://www.openligadb.de/api";
    }
    
    /**
     * Makes an api request call to a url with the given option parameters
     * 
     * @param string $url
     * @param array $options
     * @return \GuzzleHttp\Message\ResponseInterface
     */
    private function fetch($url, $options = array()) {
        return $this->traitFetch($url,$this->setDefaultHeaders($options));
    }
    
    /**
     * Returns the teams available for a league for a particular
     * season
     * 
     * @param string $league
     * @param integer $year
     * @return array<object>
     */
    public function availableTeams($league,$year){
        
        $teams = $this->fetch("getavailableteams/$league/$year");
        return $this->respondOrDefault($teams,[]);  
        
    }
    
    
    /**
     * Returns the matchdata for a league, for a year, for a matchday
     * 
     * @param string $league
     * @param int $year
     * @param int $groupOrderId
     * @return array<object>
     */
    public function matchData($league,$year,$groupOrderId){  
        $fixtures = $this->fetch("getmatchdata/$league/$year/$groupOrderId");
        return $this->respondOrDefault($fixtures,[]);
    }
    
    /**
     * Returns all the matches for a leage for a year/season
     * 
     * @param string $league
     * @param int $year
     * @return array<object>
     */
    public function seasonMatchData($league,$year){
        $matches = $this->fetch("getmatchdata/$league/$year");
        return $this->respondOrDefault($matches, []);
    }
    
    /**
     * Returns the time when the data for a league, for a year, for a matchday
     * was last changed as a string
     * 
     * @param string $league
     * @param int $year
     * @param int $groupOrderId
     * @return string|null
     */
    public function dataLastChanged($league,$year,$groupOrderId){
        $changeDate = $this->fetch("getlastchangedate/$league/$year/$groupOrderId");
        return $this->respondOrDefault($changeDate, null);
    }
    
    /**
     * Returns the details of the current group/matchday
     * 
     * @param int $league
     * @return object|null
     */
    public function currentGroup($league){
        $group = $this->fetch("getcurrentgroup/$league"); 
        return $this->respondOrDefault($group,null); 
    }
    
    /**
     * Gets the current match data for aleague
     * 
     * @param string $league
     * @return array<object>
     */
    public function currentMatchData($league){        
        $matches = $this->fetch("getmatchdata/$league");
        return $this->respondOrDefault($matches, []);     
    }
    
    /**
     * Decodes the body contents of the response
     * 
     * @param \GuzzleHttp\Message\ResponseInterface $response
     * @return mixed
     */
    private function decodeResponse($response){
        return json_decode($response->getBody()->getContents());
    }
    
    /**
     * Sets the authorization headers for a request
     * 
     * @param  array $requestOptions the request options
     * @return array the request options with authorization headers
     */
    private function setDefaultHeaders($requestOptions)
    { 
        if (array_key_exists('headers', $requestOptions)) {
            // if some headers are already set for the request
            // then merge the default headers in with these
            $requestOptions['headers'] = array_merge(
                $requestOptions['headers'], $this->defaultHeaders
            );
        } else {
            $requestOptions['headers'] = $this->defaultHeaders;
        }

        return $requestOptions;
    }
    
    /**
     * 
     * @param \GuzzleHttp\Message\ResponseInterface $response
     * @param mixed $default the default value to return
     * @param function $callback the method to unpack the contents of the response
     * @return type
     */
    private function respondOrDefault($response,$default,$callback = "decodeResponse"){
        
        if($response->getStatusCode() === 200){
            return call_user_func([$this,$callback], $response);
        }
        
        return $default;
    }
       
}

# OpenligaDB Test
Openliga DB test submission

## Pre-requisites
- PHP 5.6 or higher
- All dependencies for Laravel:5.4.*, find them [here](https://laravel.com/docs/5.4/installation)

## Setting Up
- Clone the repository to a desired location on your system
- Install dependencies with [composer](https://getcomposer.org/)
- Open a terminal at the folder location on your system
- Run `php artisan serve`
- Visit localhost:8000 or the [host:port] set up by artisan serve

## Troubleshooting
If you have problems accessing the application :-

- run the commands `php artisan cache:clear` , `php artisan route:clear` and `php artisan config:clear` from the terminal while at the application directory root
- run a `chmod 755 -R` on the `storage` and `app` directories
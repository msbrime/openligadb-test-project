<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name("home");

Route::group(["prefix" => "teams"],function(){
    Route::get("/{league}/{teamId}","TeamsController@show")->name("team");
    Route::get("/{league}","TeamsController@index")->name("teams");
});

Route::get("/fixtures/{league}/{matchday?}","FixturesController@index")->name("fixtures");



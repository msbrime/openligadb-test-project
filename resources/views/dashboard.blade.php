@extends('layout.layout')

@section('content')

<h4 class ='mb-0'>Bundesliga 1</h4>
<div class="row mb-5">
 
    <div class="col-sm-12 col-md-6">
        <h5 class = 'mt-4'>Results (Matchday {{ $bl1Matches->matchday }})</h5>
         @include("templates.match-list",["matches" =>$bl1Matches->current])
    </div>

    <div class="col-sm-12 col-md-6">
        <h5 class = 'mt-4'>Upcoming Fixtures (Matchday {{ ($bl1Matches->matchday + 1) }})</h5>
        @include("templates.match-list",["matches" =>$bl1Matches->next])
    </div>
    
</div>

<h4 class ='mb-0'>Bundesliga 2</h4>
<div class="row mb-5">
    
    <div class="col-sm-12 col-md-6">
        <h5 class = 'mt-4'>Results (Matchday {{ $bl2Matches->matchday }})</h5>
        @include("templates.match-list",["matches" =>$bl2Matches->current])
    </div>
    
    <div class="col-sm-12 col-md-6">
        <h5 class = 'mt-4'>Upcoming Fixtures (Matchday {{ ($bl2Matches->matchday + 1) }})</h5>
        @include("templates.match-list",["matches" =>$bl2Matches->next])
    </div>
    
</div>

<h4 class ='mb-0'>Bundesliga 3</h4>
<div class="row mb-5">
    
    <div class="col-sm-12 col-md-6">
        <h5 class = 'mt-4'>Results (Matchday {{ $bl3Matches->matchday }})</h5>
         @include("templates.match-list",["matches" =>$bl3Matches->current])
    </div>
    
    <div class="col-sm-12 col-md-6">
        <h5 class = 'mt-4'>Upcoming Fixtures (Matchday {{ ($bl3Matches->matchday + 1) }})</h5>
        @include("templates.match-list",["matches" =>$bl3Matches->next])
    </div>
    
</div>
@endsection
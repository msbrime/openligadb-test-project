@extends("layout.layout")

@section("content")


<h3 class = "mb-3 col-sm-12">{{ ucfirst($leagueName) }} Teams</h3>
<div class="row">
    <style>
        .team-logo{
            width:30px;
            height:30px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
            margin: 0px auto 10px;
        }
    </style>
    @foreach($teams as $team)
    <div class = "col-md-3 mb-4">
    <div class="card m-3" style="border: 1px dashed rgba(0,0,0,.425);" >
        <!--<img class="card-img-top" src="{{-- $team->iconUrl() --}}" alt="Card image cap">-->
        <a href="{{ route("team",[$league,$team->id()]) }}" class="">
        <div class="card-block p-2">
            <div class="col-sm-12 text-center">
                <div class ="team-logo" style="background-image:url('{{ $team->iconUrl() }}');"></div>
            </div>
            
            <h6 class="card-title text-center my-0">
                
                    {{ $team->name() }}
                
            </h6>
            
        </div>
            </a>
    </div>
    </div>
    @endforeach

</div>


@endsection

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>openligadb-test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset("bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">
        
    </head>
        
<body>
    
    <style>
        body {
      padding-top: 5rem;
    }
    .starter-template {
      padding: 3rem 1.5rem;
      text-align: center;
    }
    
    span.badge.badge-primary.transparent {
    background-color: transparent;
    color: #888;
    border: 1px solid #d5d5d5;
    }

    </style>

    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
        <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">OpenligaDB</a>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route("home") }}">Home </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fixtures</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="{{ route("fixtures",["bl1"]) }}">Bundesliga 1</a>
              <a class="dropdown-item" href="{{ route("fixtures",["bl2"]) }}">Bundesliga 2</a>
              <a class="dropdown-item" href="{{ route("fixtures",["bl3"]) }}">Bundesliga 3</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Teams</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="{{ route("teams",["bl1"]) }}">Bundesliga 1</a>
              <a class="dropdown-item" href="{{ route("teams",["bl2"]) }}">Bundesliga 2</a>
              <a class="dropdown-item" href="{{ route("teams",["bl3"]) }}">Bundesliga 3</a>
            </div>
          </li>
        </ul>
      </div>
      </div>
    </nav>

    <div class="container">
                
        @yield("content")

    </div><!-- /.container -->


    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="{{ asset("bootstrap/js/bootstrap.min.js") }}"></script>
    
    </body>
</html>
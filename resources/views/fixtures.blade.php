@extends('layout.layout')

@section('content')
<h3 class = "mb-3">
    Fixtures for {{ ucfirst($leagueName) }}, Matchday {{ $matchday }}
</h3>

<div class="row justify-content-center mb-4">
    
    <div class="col-sm-12 px-0">
        <div class="form-group col-sm-3 float-right">
            <select name="" class="form-control" id="" onchange="window.location = this.value">
                
                @for($day = 1; $day <= $matchdayCount; $day++)
                
                @php $selected = ($day == $matchday) ? "selected" : "" @endphp
                
                <option value="{{ route("fixtures",[$league,$day]) }}" {{$selected}}>
                    Matchday {{ $day }}
                </option>
                @endfor
            
            </select>
        </div>   
    </div>
    
    <div class="col-sm-12 align-self-center">
        @include("templates.match-list",['matches' => $matches])
    </div>
    
</div>
@endsection
<div class="list-group-item list-group-item-action">
    <div class="col-sm-12 px-0 text-center">
        <small class = 'text-muted'>{{ $match->date() }}</small>
    </div>
    <div class="col-sm-6 pl-0">
        <h6 class = 'text-left my-3'>
            {{ $match->homeTeam()->TeamName }}
           <span style ='font-size: 1.25em;margin-top: -7px;' class="badge badge-primary float-right">
               {{ $match->homeScore() >= 0 ? $match->homeScore() : "-" }}
           </span>
        </h6> 
    </div>
    <div class="col-sm-6 pr-0">
       <h6 class = 'text-right my-3'>
           <span class="badge badge-primary float-left" style ='font-size: 1.25em;margin-top: -7px;'>
            {{ $match->awayScore() >= 0 ? $match->awayScore() : "-" }}
           </span> 
           {{ $match->awayTeam()->TeamName }}
       </h6> 
    </div>
    <div class="col-sm-12 px-0 text-center">
        <small class="text-muted">{{ $match->time()}} UTC</small>
    </div>
</div>

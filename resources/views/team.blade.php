@extends("layout.layout")

@section("content")

<h3 class = "mb-4"><img src="{{$team->iconUrl()}}" 
    style="height:30px; width:30px;float:left" alt="" class="img-fluid mr-3">
    {{ ucfirst($team->name()) }}
</h3>
<div class="row mb-4">
    <div class="col-sm-3">
        <h5>Team Statistics</h5>
        <ul class="list-group">
            <li class="list-group-item justify-content-between">
                Played
                <span class="badge badge-primary transparent">{{ $stats->played }}</span>
            </li>
            <li class="list-group-item justify-content-between">
                Won
                <span class="badge badge-primary transparent">
                    {{ $stats->won }} ({{$stats->winRatio}}%)
                </span>
            </li>
            <li class="list-group-item justify-content-between">
                Lost
                <span class="badge badge-primary transparent">
                    {{ $stats->lost }} ({{$stats->lossRatio}}%)
                </span>
            </li>
            <li class="list-group-item justify-content-between">
                Draw
                <span class="badge badge-primary transparent">
                    {{ $stats->drew }} ({{ $stats->drawRatio }}%)
                </span>
            </li>
            <li class="list-group-item justify-content-between">
                Scored
                <span class="badge badge-primary transparent">{{ $stats->for }}</span>
            </li>
            <li class="list-group-item justify-content-between">
                Conceded
                <span class="badge badge-primary transparent">{{ $stats->against }}</span>
            </li>
        </ul>
    </div>
    <div class="col-sm-9">
        <h5>Season Results</h5>
         @include("templates.match-list",["matches" =>$matches])
    </div>
</div>

@endsection